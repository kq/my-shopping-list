import { ReactNode, createContext, useState } from 'react'

export type Item = {
  id: number
  name: string
  count: number
  isPurchased: boolean
}

type ShoppingContextType = {
  items: Item[]
  addItem: (name: string, count: number) => void
  deleteItem: (id: number) => void
  clearItems: () => void
  purchasedToggle: (id: number) => void
}

type ShoppingListProviderProps = {
  children: ReactNode
}

const initialItems: Item[] = [
  {
    id: +new Date() + 1,
    name: 'Kopi Bubuk',
    count: 2,
    isPurchased: true,
  },
  {
    id: +new Date() + 2,
    name: 'Gula Pasir',
    count: 1,
    isPurchased: false,
  },
  {
    id: +new Date() + 3,
    name: 'Air Mineral',
    count: 5,
    isPurchased: false,
  },
]

export const ShoppingContext = createContext<null | ShoppingContextType>(null)

export function ShoppingProvider(props: ShoppingListProviderProps) {
  const { children } = props
  const [items, setItems] = useState<Item[]>(initialItems)

  function addItem(name: string, count: number) {
    const newItem: Item = {
      id: +new Date(),
      name,
      count,
      isPurchased: false,
    }
    setItems([...items, newItem])
  }

  function deleteItem(id: number) {
    setItems(items.filter((item) => item.id !== id))
  }

  function clearItems() {
    setItems([])
  }

  function purchasedToggle(id: number) {
    setItems(
      items.map((item) => {
        if (item.id === id) {
          return { ...item, isPurchased: !item.isPurchased }
        }
        return item
      }),
    )
  }

  return (
    <ShoppingContext.Provider
      value={{ items, addItem, deleteItem, clearItems, purchasedToggle }}
    >
      {children}
    </ShoppingContext.Provider>
  )
}
