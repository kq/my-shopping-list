import { ShoppingContext } from '@/contexts/ShoppingContext'
import { useContext } from 'react'

export function useShopping() {
  const value = useContext(ShoppingContext)
  if (!value) {
    throw new Error('useShopping must be used within a ShoppingProvider')
  }
  return value
}
