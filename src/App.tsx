import './App.css'
import { ShoppingProvider } from './contexts/ShoppingContext'
import { ShoppingList } from '@/components/ShoppingList'
import { ShoppingForm } from './components/ShoppingForm'

export function App() {
  return (
    <div className="container">
      <header className="header">
        <h1 className="header__title">Catatan Belanjaku</h1>
        <span className="header__desc">3122510304 / Kenzie Wistara</span>
      </header>
      <main className="main">
        <ShoppingProvider>
          <ShoppingList />
          <ShoppingForm />
        </ShoppingProvider>
      </main>
    </div>
  )
}
