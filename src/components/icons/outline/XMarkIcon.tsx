import { IconProps } from './IconLayout'
import { IconLayout } from './IconLayout'

export function XMarkIcon(props: IconProps) {
  const { className } = props

  return (
    <IconLayout className={className}>
      <path
        strokeLinecap="round"
        strokeLinejoin="round"
        d="M6 18 18 6M6 6l12 12"
      />
    </IconLayout>
  )
}
