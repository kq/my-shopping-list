import { ReactNode } from 'react'

export type IconProps = {
  className?: string
}

type IconLayoutProps = IconProps & {
  children: ReactNode
}

export function IconLayout(props: IconLayoutProps) {
  const { className, children } = props

  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      fill="none"
      viewBox="0 0 24 24"
      strokeWidth="1.5"
      stroke="currentColor"
      className={className}
    >
      {children}
    </svg>
  )
}
