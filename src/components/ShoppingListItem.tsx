import { Item } from '@/contexts/ShoppingContext'
import { XCircleIcon } from './icons/outline/XCircleIcon'
import { cn } from '@/utils/cn'
import { useShopping } from '@/hooks/useShopping'

type ShoppingListItemProps = {
  item: Item
}

export function ShoppingListItem(props: ShoppingListItemProps) {
  const { id, count, name, isPurchased } = props.item
  const { purchasedToggle, deleteItem } = useShopping()

  return (
    <div className="shopping-list__item">
      <div className="shopping-list__item-content">
        <input
          id={`itemCheckbox-${id}`}
          type="checkbox"
          className="shopping-list__item-checkbox"
          title="Tandai telah dibeli"
          checked={isPurchased}
          onChange={() => purchasedToggle(id)}
        />
        <label
          htmlFor={`itemCheckbox-${id}`}
          className={cn(
            'shopping-list__item-name',
            isPurchased && 'shopping-list__item-name--purchased',
          )}
        >
          {count} {name}
        </label>
      </div>
      <button
        type="button"
        className="shopping-list__item-delete"
        title="Hapus barang"
        onClick={() => deleteItem(id)}
      >
        <XCircleIcon className="shopping-list__item-delete-icon shopping-list__item-delete-icon--red" />
      </button>
    </div>
  )
}
