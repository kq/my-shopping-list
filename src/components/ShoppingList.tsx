import { useState } from 'react'
import { useShopping } from '@/hooks/useShopping'
import { XMarkIcon } from '@/components/icons/outline/XMarkIcon'
import { ShoppingListItem } from './ShoppingListItem'
import { cn } from '@/utils/cn'
import { Item } from '@/contexts/ShoppingContext'

export function ShoppingList() {
  const { items, clearItems } = useShopping()
  const [sortBy, setSortBy] = useState('input')
  const itemCount = items.length
  const purchasedItemCount = items.filter((item) => item.isPurchased).length
  const percentagePurchased = (purchasedItemCount / itemCount) * 100

  let sortedItems: Item[]

  switch (sortBy) {
    case 'name':
      sortedItems = items.slice().sort((a, b) => a.name.localeCompare(b.name))
      break
    case 'count':
      sortedItems = items.slice().sort((a, b) => b.count - a.count)
      break
    case 'check':
      sortedItems = items
        .slice()
        .sort((a, b) =>
          a.isPurchased === b.isPurchased ? 0 : a.isPurchased ? -1 : 1,
        )
      break
    default:
      sortedItems = items
      break
  }

  return (
    <div className="shopping-list">
      <header className="shopping-list__header">
        <div className="shopping-list__sort">
          <label htmlFor="sort" className="shopping-list__sort-label">
            Urutkan berdasarkan
          </label>
          <select
            id="sort"
            className="shopping-list__sort-select"
            onChange={(e) => setSortBy(e.target.value)}
          >
            <option value="input">Urutan input</option>
            <option value="name">Nama barang</option>
            <option value="count">Jumlah barang</option>
            <option value="check">Ceklis</option>
          </select>
        </div>
        <button
          type="button"
          className="shopping-list__clear"
          title="Bersihkan daftar belanja"
          onClick={clearItems}
        >
          <XMarkIcon className="shopping-list__clear-icon" /> Bersihkan
        </button>
      </header>
      <main className="shopping-list__main">
        {sortedItems.length ? (
          sortedItems.map((item) => (
            <ShoppingListItem key={`item-${item.id}`} item={item} />
          ))
        ) : (
          <span className="shopping-list__empty">
            Daftar belanja masih kosong!
          </span>
        )}
      </main>
      <footer
        className={cn(
          'shopping-list__footer',
          !items.length && 'shopping-list__footer--hide',
        )}
      >
        <p>
          Ada {itemCount} barang di daftar belanjaan, {purchasedItemCount}{' '}
          barang sudah dibeli ({percentagePurchased.toFixed()}%)
        </p>
      </footer>
    </div>
  )
}
