import { FormEvent, useRef } from 'react'
import { useShopping } from '@/hooks/useShopping'

export function ShoppingForm() {
  const { addItem } = useShopping()
  const itemNameInputRef = useRef<HTMLInputElement>(null)
  const itemCountInputRef = useRef<HTMLInputElement>(null)

  function onSubmit(e: FormEvent<HTMLFormElement>) {
    e.preventDefault()
    addItem(
      itemNameInputRef.current!.value,
      parseInt(itemCountInputRef.current!.value, 10),
    )
    itemNameInputRef.current!.value = ''
    itemCountInputRef.current!.value = '1'
  }

  return (
    <aside className="shopping-form">
      <h4 className="shopping-form__title">Hari ini belanja apa?</h4>
      <form className="shopping-form__form" onSubmit={onSubmit}>
        <div className="shopping-form__form-group">
          <label className="shopping-form__label">Nama barang</label>
          <input
            type="text"
            className="shopping-form__input"
            required
            ref={itemNameInputRef}
          />
        </div>
        <div className="shopping-form__form-group">
          <label className="shopping-form__label">Jumlah barang</label>
          <input
            type="number"
            className="shopping-form__input"
            defaultValue="1"
            min="1"
            required
            ref={itemCountInputRef}
          />
        </div>
        <button type="submit" className="shopping-form__submit">
          Tambah
        </button>
      </form>
    </aside>
  )
}
